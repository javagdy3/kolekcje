package com.sda.javagdy5.kolekcje.typgeneryczny;

public class Para<L, P> {
    private L lewa;
    private P prawa;

    public Para(L lewa, P prawa) {
        this.lewa = lewa;
        this.prawa = prawa;
    }

    public L getLewa() {
        return lewa;
    }

    public void setLewa(L lewa) {
        this.lewa = lewa;
    }

    public P getPrawa() {
        return prawa;
    }

    public void setPrawa(P prawa) {
        this.prawa = prawa;
    }
}