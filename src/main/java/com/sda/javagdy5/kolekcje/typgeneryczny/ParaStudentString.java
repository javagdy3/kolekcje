package com.sda.javagdy5.kolekcje.typgeneryczny;

public class ParaStudentString {
    private Student lewa;
    private String prawa;

    public ParaStudentString(Student lewa, String prawa) {
        this.lewa = lewa;
        this.prawa = prawa;
    }

    public Student getLewa() {
        return lewa;
    }

    public void setLewa(Student lewa) {
        this.lewa = lewa;
    }

    public String getPrawa() {
        return prawa;
    }

    public void setPrawa(String prawa) {
        this.prawa = prawa;
    }
}
