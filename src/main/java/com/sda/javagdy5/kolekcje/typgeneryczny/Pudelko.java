package com.sda.javagdy5.kolekcje.typgeneryczny;

public class Pudelko<T> {
    private T cosWSrodku;

    public boolean czyPudelkoJestPuste(){
        return cosWSrodku == null;
    }

    public T  zwrocZawartosc(){
        return cosWSrodku;
    }

    public void setCosWSrodku(T cosWSrodku) {
        this.cosWSrodku = cosWSrodku;
    }
}
