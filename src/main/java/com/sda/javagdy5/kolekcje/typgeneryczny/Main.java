package com.sda.javagdy5.kolekcje.typgeneryczny;

public class Main {
    public static void main(String[] args) {
        Pudelko<String> p = new Pudelko<String>();

        p.setCosWSrodku("Paweł");

        String zwroconaWartosc = p.zwrocZawartosc();
        System.out.println(zwroconaWartosc);
//        if(zwroconaWartosc instanceof String) {
//            String zrzutowanaZawartosc = (String) zwroconaWartosc;

//        }

        Para<String, Integer> para = new Para<>("5", 5);
        Para<Integer, String> paraOdwrotnie = new Para<>(5, "5");

    }
}
