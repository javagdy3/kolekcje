package com.sda.javagdy5.kolekcje.typgeneryczny;

public class ParaStudentSamochod {
    private Student lewa;
    private Samochod prawa;

    public ParaStudentSamochod(Student lewa, Samochod prawa) {
        this.lewa = lewa;
        this.prawa = prawa;
    }

    public Student getLewa() {
        return lewa;
    }

    public void setLewa(Student lewa) {
        this.lewa = lewa;
    }

    public Samochod getPrawa() {
        return prawa;
    }

    public void setPrawa(Samochod prawa) {
        this.prawa = prawa;
    }
}
