package com.sda.javagdy5.kolekcje.zadania;

public class VarArgsExample {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 1, 1};
        int suma = dodajLiczby(1, 3, 5, 6, 7, 74, 2, 1, 3, 3, 5, 2, 1);
        System.out.println("Suma: " + suma);
    }

    private static int dodajLiczby(int... varargsToTablica) {
        int suma = 0;

        for (int i = 0; i < varargsToTablica.length; i++) {
            suma += varargsToTablica[i];
        }
        return suma;
    }
}
