package com.sda.javagdy5.kolekcje.zadania.zad5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.addAll(Arrays.asList(
                new Student("123", "Marian", "Kowalski", Plec.MEZCZYZNA),
                new Student("342", "Katarzyna", "Kowalska", Plec.KOBIETA),
                new Student("568", "Włodek", "Smith", Plec.MEZCZYZNA),
                new Student("367", "Marcin", "Nowak", Plec.MEZCZYZNA),
                new Student("273", "Olek", "Dębski", Plec.MEZCZYZNA),
                new Student("299", "Juliusz", "Brzęczyszczykiewicz", Plec.MEZCZYZNA),
                new Student("929", "Maryla", "Dębska", Plec.KOBIETA)
        ));

        // 1
        System.out.println(students);

        // 2
        for (Student student : students) {
            System.out.println(student);
        }

        // 3
        System.out.println("Tylko kobiety:");
        for (Student student : students) {
            if (student.getPlec() == Plec.KOBIETA) {
                System.out.println(student);
            }
        }

        // 4
        System.out.println("Tylko mezczyzn:");
        for (Student student : students) {
            if (student.getPlec() == Plec.MEZCZYZNA) {
                System.out.println(student);
            }
        }

        // 5
        System.out.println("Indeksy:");
        for (Student student : students) {
            System.out.println(student.getIndeks());
        }


    }
}
