package com.sda.javagdy5.kolekcje.zadania.mapy.zad1;

import lombok.*;

@Data
//@RequiredArgsConstructor// - konstruktor niezbędnych parametrów konstruktora
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private long nrIndeksu;
    private String imie;
    private String nazwisko;
}


