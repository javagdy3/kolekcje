package com.sda.javagdy5.kolekcje.zadania.zbiory.zad1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        Set<Integer> numberSet = new TreeSet<>(Arrays.asList(7,3,11,12,10,7,3,4,12,12,300,12,40,55));

        System.out.println("Ilość: " + numberSet.size());

        printSet(numberSet);

        numberSet.remove(10);
        numberSet.remove(12);

        System.out.println("Ilość: " + numberSet.size());
        printSet(numberSet);

    }

    private static void printSet(Set<Integer> numberSet) {
        System.out.println();
        for (Integer wartosc : numberSet) {
            System.out.print(wartosc + " ");
        }
        System.out.println();
    }
}
