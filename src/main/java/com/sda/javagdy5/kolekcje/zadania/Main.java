package com.sda.javagdy5.kolekcje.zadania;

import com.sda.javagdy5.kolekcje.zadania.zad6.Dziennik;
import com.sda.javagdy5.kolekcje.zadania.zad6.Student;

import java.util.ArrayList;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {

        Dziennik dziennik = new Dziennik();
        dziennik.dodajStudenta(new Student(new ArrayList<>(), "Marianek", "Marianski", "124"));

        // skorzystaliśmy z metody zwróć studenta.
        Optional<Student> stOpt = dziennik.zwrocStudenta("123");

        // is present = wykorzystanie. Sprawdza czy w pudełku jest obiekt. jeśli jest - to go wyciągamy
        if(stOpt.isPresent()) {
            Student st = stOpt.get(); // wyciągnięcie z pudełka obiektu.
            System.out.println(st.getName());
        }else{
            System.out.println("Brak.");
        }
    }
}
