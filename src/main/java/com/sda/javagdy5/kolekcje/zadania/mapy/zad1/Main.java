package com.sda.javagdy5.kolekcje.zadania.mapy.zad1;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<Long, Student> studentHashMap = new HashMap<>();
        studentHashMap.put(100200L, new Student(100200, "Pawel", "Kura"));
        studentHashMap.put(100400L, new Student(100400, "Pawelo", "Kurczak"));
        studentHashMap.put(100600L, new Student(100600, "Pawele", "Pieczeń"));
        studentHashMap.put(100800L, new Student(100800, "Paweleeeeeee", "Hehe"));
        studentHashMap.put(101000L, new Student(101000, "Pawelff", "Garaz"));

        if (studentHashMap.containsKey(100200L)) {
            System.out.println("JEST!");
        }
        System.out.println(studentHashMap.get(100400L));
        System.out.println("Liczba studentów: "+ studentHashMap.size());

        for (Student student : studentHashMap.values()) {
            System.out.println(student);
        }



    }
}
