package com.sda.javagdy5.kolekcje.zadania.zad1;

public class Main {
    public static void main(String[] args) {
        Beben b = new Beben();
        Pianino p = new Pianino();
        Gitara g = new Gitara();

        Instrumentalny[] instrumenty = new Instrumentalny[]{b, p, g};
        for (int i = 0; i < instrumenty.length; i++) {
            instrumenty[i].graj();
        }
    }
}
