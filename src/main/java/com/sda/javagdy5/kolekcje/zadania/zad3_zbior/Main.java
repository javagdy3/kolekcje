package com.sda.javagdy5.kolekcje.zadania.zad3_zbior;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>(Arrays.asList(
                "10030",
                "3004",
                "4000",
                "12355",
                "12222",
                "67888",
                "111200",
                "225355",
                "2222",
                "1111",
                "3546",
                "138751",
                "235912"
                ));
        System.out.println(stringList);

        System.out.println("Indeks 138751: " + stringList.indexOf("138751"));
        System.out.println("Contains 67888: " + stringList.contains("67888"));
        System.out.println("Contains 67889: " + stringList.contains("67889"));

        System.out.println("Remove 67888: " + stringList.remove("67888"));
        System.out.println("Remove 67889: " + stringList.remove("67889"));

        for (String s : stringList) {
            System.out.println(s);
        }

        System.out.println(stringList);


        stringList.remove(0);
    }
}
