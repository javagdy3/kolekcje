package com.sda.javagdy5.kolekcje.zadania.zad6;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Dziennik {
    private List<Student> students = new ArrayList<>();

    public void dodajStudenta(Student st) {
        students.add(st);
    }

    public void usunStudenta(Student st) {
        students.remove(st);
    }

    /**
     * Metoda zwraca Optional (pudełko) ze studentem. Jeśli student jest dostępny i został znaleziony to zostanie
     * zwrócony w formie pudełka. W pudełku mamy studenta i musimy przed wyciągnięciem sprawdzić czy tam jest.
     *
     * @param nrIndeksu
     * @return
     */
    public Optional<Student> zwrocStudenta(String nrIndeksu) {
        for (Student student : students) {
            if (student.getIndeksNumber().equals(nrIndeksu)) {
//                return student;
                return Optional.of(student); // Jeśli jest wynik, to zwracamy go przez Optional.of
            }
        }
//        return null;
//        throw new RuntimeException();
        return Optional.empty(); // jeśli nie chcemy / nie mamy wyniku do zwrócenia, to zwracamy empty
    }

    public void usunStudenta(String nrIndeksu) {
        for (Student student : students) {
            if (student.getIndeksNumber().equals(nrIndeksu)) {
                students.remove(student);
                break;
            }
        }
    }

    public boolean czyIstniejeStudentOPodanymIndeksie(String index) {
        return zwrocStudenta(index).isPresent();
    }

    public void listujWszystkichStudentow() {
        for (Student student : students) {
            System.out.println(student);
        }
    }

}
