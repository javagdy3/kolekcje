package com.sda.javagdy5.kolekcje.zadania.mapy.zad1;

public class MainException {
    public static void main(String[] args) {
        System.out.println(":P main");
        metoda1();

        System.out.println("znowu  :P main");
    }

    private static void metoda1() {
        try {
            System.out.println("metoda1");
            metoda2();
        } catch (Exception e) {
            System.out.println("Yo yo java 5:" + e.getMessage());
        }
    }

    private static void metoda2() {
        System.out.println("metoda2");
        metoda3();

    }

    private static void metoda3() {
        System.out.println("metoda3");
         throw new RuntimeException("Akuku!");
    }
}
