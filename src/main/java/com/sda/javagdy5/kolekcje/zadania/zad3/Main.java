package com.sda.javagdy5.kolekcje.zadania.zad3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<Integer> liczby = new ArrayList<>();

        // tworzymy generator
        Random generator = new Random();
        for (int i = 0; i < 10; i++) {
            // generator ma wylosowac liczbę i przekazać ją 
            // bezpośrednio do metody add - która dodaje do listy
            liczby.add(generator.nextInt(50));
        }

        double suma = 0.0;
        for (Integer wartosc : liczby) {
            suma += wartosc;
        }
//        for (int i = 0; i < liczby.size(); i++) {
//            suma +=liczby.get(i);
//        }
        System.out.println("Suma to: " + suma);
        System.out.println("Średnia to: " + (suma / liczby.size()));

        // konstruktor kopiujący - weź liczby z listy "liczby" i
        // przekopiuj je do drugiej, nowej listy
        List<Integer> kopia = new ArrayList<>(liczby);

        Collections.sort(kopia);

        double mediana = 0.0;
        if (kopia.size() % 2 == 0) {
            mediana = (kopia.get(kopia.size() / 2 - 1) + kopia.get(kopia.size() / 2)) / 2.0;
        } else {
            mediana = (kopia.get(kopia.size() / 2));
        }
        System.out.println("Dla tablicy: " + kopia);
        System.out.println("Mediana to: " + mediana);

        int min = liczby.get(0), max = liczby.get(0);
        for (int i = 1; i < liczby.size(); i++) {
            if (min > liczby.get(i)) {
                min = liczby.get(i);
            }
            if (max < liczby.get(i)) {
                max = liczby.get(i);
            }
        }
        System.out.println("Min : " + min);
        System.out.println("Max : " + max);

        int indexMax = -1;
        for (int i = 0; i < liczby.size(); i++) {
            if (liczby.get(i) == max) {
                indexMax = i;
                break;
            }
        }

        System.out.println(liczby);

        System.out.println("Indeks maximum = " + indexMax);
        System.out.println("Index max: " + liczby.indexOf(max));
        System.out.println("Index min: " + liczby.indexOf(min));

    }
}
