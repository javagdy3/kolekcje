package com.sda.javagdy5.kolekcje.zadania.zad2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        Random generator = new Random();

        for (int i = 0; i < 5; i++) {
            System.out.println("Podaj element:");
            int nowyElement = scanner.nextInt();

            list.add(nowyElement);
        }

        for (int i = 0; i < 5; i++) {
            int nowyElement = generator.nextInt();

            list.add(nowyElement);
        }

        // 5, 10, 15, 20, 25
        for (int i = 0; i < list.size(); i++) {
            Integer wartosc = list.get(i); //0, 1
            // 5, 10
            System.out.println(wartosc);
        }
        System.out.println();
        for (Integer wartosc : list) {
            System.out.println(wartosc);
        }
        System.out.println();

        System.out.println(list); // [5, 10, 15, 20, 25]


    }
}
