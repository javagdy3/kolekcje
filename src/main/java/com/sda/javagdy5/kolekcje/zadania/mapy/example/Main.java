package com.sda.javagdy5.kolekcje.zadania.mapy.example;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<String, Osoba> mapaObywateli = new HashMap<>();

        mapaObywateli.put("87123687126387",
                new Osoba("87123687126387", "Marian", "Kowalski"));
        mapaObywateli.put("87123687126383",
                new Osoba("87123687126383", "Ola", "Kawska"));

        System.out.println(mapaObywateli);

        System.out.println("Ty: " + mapaObywateli.get("87123687126383"));  //Ola
        System.out.println("Ty: " + mapaObywateli.get(87123687126383L));   //null
        System.out.println("Ty: " + mapaObywateli.get(871236871));         //null

        // ta kolekcja może być iterowana przez foreach
        Set<String> klucze = mapaObywateli.keySet();

        // ta kolekcja może być iterowana przez foreach
        Collection<Osoba> values = mapaObywateli.values();

        // entry posiada getter dla klucza - getKey()
        // entry posiada getter dla wartości - getValue()
        for (Map.Entry<String, Osoba> wpisWMapie : mapaObywateli.entrySet()) {
            System.out.println(wpisWMapie.getKey());
            System.out.println(wpisWMapie.getValue());
        }


    }
}
