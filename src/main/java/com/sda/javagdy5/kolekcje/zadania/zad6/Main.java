package com.sda.javagdy5.kolekcje.zadania.zad6;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Dziennik dziennik = new Dziennik();

        dziennik.dodajStudenta(new Student(new ArrayList<>(), "Pawel", "Gawel", "12345"));
        dziennik.dodajStudenta(new Student(new ArrayList<>(), "Pawel", "Gawel", "12347"));
        dziennik.dodajStudenta(new Student(new ArrayList<>(), "Pawel", "Gawel", "12344"));


        dziennik.usunStudenta("12347");
        dziennik.listujWszystkichStudentow();
    }
}
