package com.sda.javagdy5.kolekcje.zadania.zad4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<String> listStrings = new ArrayList<>();

        Random generator = new Random();

        for (int i = 0; i < 10; i++) {
            listStrings.add(String.valueOf(generator.nextInt(100)));
        }

        double suma = 0;
        for (int i = 0; i < listStrings.size(); i++) {
            String element = listStrings.get(i);

            int parsed = Integer.valueOf(element);
            suma += parsed;
        }
        double srednia = suma / listStrings.size();
        System.out.println("Średnia : " + srednia);

//        ##############################################
//        złe
//        for (String elementValue : listStrings) {
//            if (Integer.parseInt(elementValue) > srednia) {
//                listStrings.remove(elementValue);
//            }
//        }
        System.out.println(listStrings);
//        iterator jest klasą dostępną w każdej kolekcji
//        każda kolekcja pozwala pobrać obiekt do iteracji.
//
        Iterator<String> iterator = listStrings.iterator();
        // dopóki element posiada następny (czyli jest co dalej iterować)
        while (iterator.hasNext()) { // dopóki jest następny element

            // zwróć uwagę że next przechodzi do następnego elementu i go zwraca.
            String nextElement = iterator.next();

            // jeśli kryterium jest spełnione, to usun.
            if (Integer.parseInt(nextElement) > srednia) {
                iterator.remove();
            }
        }
        System.out.println(listStrings);

//        ##############################################

        // wypisz wszystko na ekran
        System.out.println(listStrings);

//         przekopiuj do tablicy
        String[] arrayString = new String[listStrings.size()];
        for (int i = 0; i < listStrings.size(); i++) {
            arrayString[i] = listStrings.get(i);
        }
//         przepisana tablica
        // drugi sposób, wykorzystując metodę z klasy List!!!
        String[] arrayString2 = new String[listStrings.size()];
        arrayString2 = listStrings.toArray(arrayString2);
        for (int i = 0; i < arrayString2.length; i++) {
            System.out.println(arrayString2[i]);
        }
        System.out.println("Przepisana ^^ ");


    }
}
