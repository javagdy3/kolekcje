package com.sda.javagdy5.kolekcje.zadania.zad5;

public class Student {
    private String indeks;
    private String imie;
    private String nazwisko;
    private Plec plec;

    public Student(String indeks, String imie, String nazwisko, Plec plec) {
        this.indeks = indeks;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;
    }

    public String getIndeks() {
        return indeks;
    }

    public void setIndeks(String indeks) {
        this.indeks = indeks;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public Plec getPlec() {
        return plec;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }

    @Override
    public String toString() {
        return "Student{" +
                "indeks='" + indeks + '\'' +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", plec=" + plec +
                '}';
    }
}
