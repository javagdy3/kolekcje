package com.sda.javagdy5.kolekcje.interfejsy;

public class Drzwi implements IOtwieralny, IZamykalnyNaKlucz {
    private boolean zamknięte;
    private boolean zakluczone;

    @Override
    public void otworz() {
        if (zakluczone) {
            System.out.println("Nie mogę otworzyć, drzwi są zamknięte na klucz.");
        } else if (zamknięte) {
            System.out.println("Otwieram drzwi.");
            zamknięte = false;
        } else {
            System.out.println("Nie mogę otworzyć otwartych drzwi.");
        }
    }

    @Override
    public void zamknij() {
        if (!zamknięte) {
            System.out.println("Zamykam drzwi");
            zamknięte = true;
        } else {
            System.out.println("Nie mogę ponownie zamknąć drzwi. Drzwi są zamknięte.");
        }
    }

    @Override
    public void zaklucz() {
        if (zamknięte && !zakluczone) {
            System.out.println("Zamykam drzwi na klucz.");
            zakluczone = true;
        }else{
            System.out.println("Nie mogę zakluczyć otwartych ani zamkniętych na klucz drzwi.");
        }
    }
}
