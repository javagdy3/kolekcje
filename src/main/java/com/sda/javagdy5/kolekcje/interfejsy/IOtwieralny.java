package com.sda.javagdy5.kolekcje.interfejsy;

public interface IOtwieralny {
    // nie jest to pole, tylko stała!
    public static final int POLE = 3;

    default void  otworz() {
        // implementacja metody która wykona się jeśli metody nie nadpiszemy
        // metoda defaultowa jest od niedawna (java 8) i powoduje że nie musimy
        // implementować tej metody jeśli implementujemy interfejs.
    }
    void zamknij();

}
